from onnxparser.onnxparser import onnxextract
import os
import argparse

def st_performance_evaluation(onnx_file):

  net_file = os.path.join("./", 'Net.txt')
  
  onnxextract(model_file= onnx_file,
              path="./",
              target="text")


  network_structure=decode_net(net_file)

  layer_IF = network_structure[0]
  layer_OF = network_structure[1]
  layer_IH = network_structure[2]
  layer_IW = network_structure[3]
  layer_OH = network_structure[4]
  layer_OW = network_structure[5]
  layer_kernel = network_structure[6]
  layer_pad = network_structure[7]
  gemm_IF = network_structure[8]
  gemm_OF = network_structure[9]

  Texe = 0
  Wfoot = 0
  Afoot = 0

  #network performance estimation
  for i in range(len(layer_IF)):
    Texe_conv, wei_foot, act_foot = convlayer_evaluation_sensortile(layer_IF[i],layer_OF[i],layer_IH[i],layer_IW[i],layer_OH[i],layer_OW[i],layer_kernel[i],layer_pad[i])
    Texe = Texe + Texe_conv
    Wfoot = Wfoot + wei_foot
    if Afoot < (act_foot*2):
      Afoot = act_foot*2

  for i in range(len(gemm_IF)):
    Texe_gemm, wei_foot, act_foot = gemmlayer_evaluation_sensortile(gemm_IF[i],gemm_OF[i])
    Texe = Texe + Texe_gemm
    Wfoot = Wfoot + wei_foot
    if Afoot < (act_foot*2):
      Afoot = act_foot*2

  print("\n\nNETWORK EXECUTION TIME " + str(Texe))
  print("\n\nWEIGHTS FOOTPRINT " + str(Wfoot) +"\nACT FOOTPRINT " + str(Afoot))
  
  results = {'execution_time' : Texe,
             'energy'         : 0,
             'processors'     : 0,
             'memory'         : Afoot + Wfoot
  }
  
  return results



def decode_net(net_file):

    layer_OF = []
    layer_IF = []
    layer_IH = []
    layer_IW = []
    layer_OH = []
    layer_OW = []
    layer_kernel = []
    layer_pad = []
    gemm_IF = []
    gemm_OF = []

    f=open(net_file,"r")

    for line in f:

      if len(line.split("	")) == 11:
        Layer, Operator, IF, IH, IW, OF, OH, OW, kernel, pad, el = line.split("	")
        if "Conv" in Operator:

           k0temp, k1temp= kernel.split("[")
           k,k0temp=k1temp.split("]")
           k0,k1=k.split(",")
           pad0temp, pad1temp= pad.split("[")
           p,pad0temp=pad1temp.split("]")
           pad0,pad1,pad2,pad3=p.split(",")

           layer_IF.append(int(IF))
           layer_IH.append(int(IH))
           layer_IW.append(int(IW))
           layer_OH.append(int(OH))
           layer_OW.append(int(OW))
           layer_OF.append(int(OF))
           layer_kernel.append([int(k0),int(k1)])
           layer_pad.append([int(pad0),int(pad1),int(pad2),int(pad3)])
        if "Gemm" in Operator:
           gemm_IF.append(int(IF)*int(IH)*int(IW))
           gemm_OF.append(int(OF))

    return([layer_IF,layer_OF,layer_IH,layer_IW,layer_OH,layer_OW,layer_kernel,layer_pad,gemm_IF,gemm_OF])


def convlayer_evaluation_sensortile(IF,OF,IH,IW,OH,OW,fs,pad):

    #MEASURED ROOFLINE#
    data_bytes = 1 
    max_perf = 0.05
    tot_bw = 0.32
    freq = 80
 
    print("\nConv Layer")
    print("IF " + str(IF) + " OF " + str(OF) + " IH " + str(IH) + " IW " + str(IW) + " OH " + str(OH) +  " OW " + str(OW) + " Kx " + str(fs[0]) + " Ky " + str(fs[1]))   

    #OPS evaluation
    MOPS=float((IF*OF*(OW)*(OH)*fs[0]*fs[1]*2)/1000000)

    print("MOPS " + str(MOPS))

    #min memory traffic
    Ibyte = IF*(IH)*(IW)*data_bytes
    Wbyte = (IF*OF*fs[0]*fs[1]+OF)*data_bytes
    Obyte = OF*(OH)*(OW)*data_bytes
    Actbyte = max([Ibyte,Obyte])
    Mintraffic = Ibyte + Obyte + Wbyte

    print("Itraffic " + str(Ibyte))
    print("Wtraffic " + str(Wbyte))
    print("Otraffic " + str(Obyte))

    #Simple Intensity evaluation
    SIntensity = MOPS*1000000/Mintraffic
    print("Intensity " + str(SIntensity))

    #if layer intensity falls in computation limited region
    perf = max_perf
    #if layer intensity falls in bandwidth limited region
    if SIntensity < (max_perf/tot_bw):
      perf=tot_bw*SIntensity

    #Texe evaluation based on SIMPLE ROOFLINE
    TexeSRoof = MOPS/float(perf)
    print("Estim Texe " + str(TexeSRoof))

    return([TexeSRoof,Wbyte,Actbyte])


def gemmlayer_evaluation_sensortile(IF,OF):

    #MEASURED ROOFLINE#
    data_bytes = 1 
    max_perf = 0.02
    tot_bw = 0.32
    freq = 80

    print("\nGemm Layer")
    print("IF " + str(IF) + " OF " + str(OF) + " IH ")

    #OPS evaluation
    MOPS=float((IF*OF)/1000000)

    print("MOPS " + str(MOPS))

    #min memory traffic
    Ibyte = IF*data_bytes
    Wbyte = (IF*OF+OF)*data_bytes
    Obyte = OF*data_bytes
    Actbyte = max([Ibyte,Obyte])
    Mintraffic = Ibyte + Obyte + Wbyte

    print("Itraffic " + str(Ibyte))
    print("Wtraffic " + str(Wbyte))
    print("Otraffic " + str(Obyte))

    #Simple Intensity evaluation
    SIntensity = MOPS*1000000/Mintraffic
    print("Intensity " + str(SIntensity))

    #if layer intensity falls in computation limited region
    perf = max_perf
    #if layer intensity falls in bandwidth limited region
    if SIntensity < (max_perf/tot_bw):
      perf=tot_bw*SIntensity

    #Texe evaluation based on SIMPLE ROOFLINE
    TexeSRoof = MOPS/float(perf)
    print("Estim Texe " + str(TexeSRoof))

    return([TexeSRoof,Wbyte,Actbyte])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Compute the sensortile performance evaluation.')
    parser.add_argument('onnx_file', type=str, help='Path to onnx model')

    args = parser.parse_args()

    st_performance_evaluation(args.onnx_file)
