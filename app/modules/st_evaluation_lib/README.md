# ST_perfeval

This code is intended to be used to produce performance estimation on SensorTile

## Usage

```
python3 ST_perfeval.py path_to_network_file

```

ARGS:

path_to_network_file, input file with network structure 

#provided input files

Net.txt contains sample description of a network model

it is produced upon execution of onnxparser on the onnx model describing the network to be evaluated : onnxparser/output_path/

#output

Upon execution produces:

  1. Network execution time estimation (considering 8 bit execution)
  2. Weights storage requirements (8 bit)
  3. Activations storage requirements (8 bit - smart allocation exploiting equal sized input and output buffers) 
