import unittest
from mongoengine import *
import os
import tempfile
from flask import Flask, url_for, redirect

#
# This file contains different examples of pytests. Please extend it to cover all your code.
#
# IMPORTANT! -> Testing functions name should start with:_ test_*
#
#
# To execute it:    python -m pytest test.py -s

class TestGeneratedApp (unittest.TestCase):
    # Test import pagackes
    def test_modules_import(self):
        print("--> Test import packages")
        testmodules = [
            'api',
            'models.models',
            'db.mongo',
            'settings.settings'
        ]

        suite = unittest.TestSuite()

        for t in testmodules:
            try:
                # If the module defines a suite() function, call it to get the suite.
                mod = __import__(t, globals(), locals(), ['suite'])
                suitefn = getattr(mod, 'suite')
                suite.addTest(suitefn())
            except (ImportError, AttributeError):
                # else, just load all the test cases from the module.
                suite.addTest(unittest.defaultTestLoader.loadTestsFromName(t))

        unittest.TextTestRunner().run(suite)


    # Test DB connection and User class behaviour
    def test_db_connection(self):

        import models
        print("--> Test DB connection")

        connection = connect('mongoenginetest', host='mongomock://localhost')
        original_pass = "hidden_pass"

        init_count = models.models.User.objects.count()
        test_object = models.models.User()
        test_object.email = "fake_email123@email.com"

        test_object.password = original_pass
        test_object.projects = ['3']
        test_object.save()

        assert models.models.User.objects.count() == init_count+1

        test_object_from_db = models.models.User.objects[init_count]

        assert test_object_from_db.email == test_object.email
        assert test_object_from_db.password != original_pass #Password is encrypted


        assert models.models.User.validate_login(test_object.password, original_pass)

        connection.drop_database('mongoenginetest')

        assert models.models.User.objects.count() == 0

        test_object = models.models.User()
        test_object.save()


        assert models.models.User.objects.count() == 1
        connection.close()

    #Test Flask API initialization
    def test_flask_api(self):
        import api
        print("--> Test Flask API")
        app = Flask(__name__, static_folder='static')
        app.register_blueprint(
            api.bp,
            url_prefix='/api')

        @app.route('/')
        def home():
            return redirect(url_for('static', filename='swagger-ui/index.html'))
        c = app.test_client()
        response1 = c.get('/wrong/url')


        assert response1._status == '404 NOT FOUND'
        assert response1.headers['Content-Type'] == 'application/json'

        response2 = c.get('/')


        assert response2.headers['Content-Type'] != response1.headers['Content-Type']
        assert response2.headers['Content-Type'].split("; ")[0] == 'text/html'
        assert response2.headers['Location'] == 'http://localhost/static/swagger-ui/index.html'
