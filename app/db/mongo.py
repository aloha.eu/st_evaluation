## Generate MongoDB models from YAML definitions
### Example: https://stackoverflow.com/questions/6311738/create-a-model-from-yaml-json-on-the-fly

from flask_mongoengine import MongoEngine
from mongoengine.connection import get_db, connect
from mongoengine import connect
import os

class MongoConnector():

    def __init__(self, app=None):
        if app is not None:
            self.mongo_engine = MongoEngine()
            self.mongo_engine.init_app(app)

        # Obtain db connection(s)
            self.db = get_db()
            #print("Database stats: ", self.db.command('dbstats'))
        else:
            # todo improve this
            print ("Connecting to: ")
            print (os.environ['MONGO_DB_NAME'])
            print (os.environ['MONGO_DB_TCP_ADDR'])
            print (os.environ['MONGO_DB_PORT'])
            self.mongo_engine = connect(os.environ['MONGO_DB_NAME'],  host=str(os.environ['MONGO_DB_TCP_ADDR']),  port=int(os.environ['MONGO_DB_PORT']))
