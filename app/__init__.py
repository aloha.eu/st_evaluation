# -*- coding: utf-8 -*-
from __future__ import absolute_import

from flask import Flask, url_for, redirect

import api
from models.models import *
from db.mongo import MongoConnector
from settings import settings
from optparse import OptionParser


def create_app():
    app = Flask(__name__, static_folder='static')
    app.register_blueprint(
    api.bp,
    url_prefix='/api')

    @app.route('/')
    def home():
        return redirect(url_for('static', filename='swagger-ui/index.html'))

    settings.get_settings(app)

    try:
        mongo = MongoConnector(app)

#        user = User()
#        user.email = "David.Solans@ca.com"

#        user.password = "hidden_pass"
#        user.projects = ['3']
#        user.save()

#        app.logger.info("Password: ", user.password)
    except Exception as ex:
        app.logger.info(str(ex))
#        app.logger.info("User %s was already created" % user.email)


    return app



if __name__ == '__main__':


    parser = OptionParser()
    parser.add_option("-p", "--port", dest="port",
                      help="Flask port", metavar="", default=5000)


    (options, args) = parser.parse_args()
    print("Options: ", options)

    option_dict = vars(options)
    port = int(option_dict['port'])

    create_app().run(debug=True, port=port, host='0.0.0.0', threaded=True)
