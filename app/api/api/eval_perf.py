# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function

from flask import Flask, request, g

from . import Resource
from .. import schemas



# the following two imports are needed for create a new connection
from settings import settings
from db.mongo import MongoConnector



#the following line imports the mongoDBengine classes from the models.py file
#each class correspond to a collection in the DB (a collection is the analogue to a table.
#An entry in the collection is the analogue of a row in the table)
from models.models import *

from mongoengine.errors import *
from mongoengine.queryset.visitor import Q
from bson import ObjectId

from rq import Queue

import subprocess
import os

from worker import *


def execute_eval (**kwargs):


  # change these paths with your default paths
  tstArchPath = os.getcwd() + os.sep + "architecture/fake_arch.json"
  tstONNXPath = os.getcwd() + os.sep + "onnx/mnist/mnist.onnx"


  #tstArchPath = "~/power_performance_evaluation/app/py_perf_eval/architecture/fake_arch.json"
  #tstONNXPath = "~/power_performance_evaluation/app/py_perf_eval/onnx/mnist/mnist.onnx"

  ONNXPath = kwargs.get("onnxPath", tstONNXPath)
  ArchPath = kwargs.get("archPath", tstArchPath)

  projID = kwargs.get("projectID", None)
  algoID = kwargs.get("algorithmID", None)

  callback_id = kwargs.get("callback_id", None)

  #import time
  # uncomment the following line to simulate a longer process
  #time.sleep(30)


  '''
  os.chdir("./py_perf_eval") # WORKAROUND: the working dir must be the one containing protobuf_interface.jar

  cmd = ['java', '-jar', 'protobuf_interface.jar', '--evaluate', ONNXPath]
  DEVNULL = os.open(os.devnull, os.O_WRONLY)
  process = subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True, stderr=DEVNULL)

  from threading import Timer

  # after this timeout the process will be killed
  timeout=60

  timer = Timer(timeout, process.kill)
  try:
    timer.start()
    stdout, stderr = process.communicate()
  finally:
    timer.cancel()

  result = stdout
  print(result)


  '''
  # connect to the DB
  app = Flask(__name__, static_folder='static')
  settings.get_settings(app)

  mongo = MongoConnector(app)
  '''

  import json

  # Convert to json the output of the java application
  try:
	  j = json.loads(result)
  except:
	  print ("Evaluation failed!")
	  return

  '''
  j={}
  j['performance'] = 10
  j['energy']=324
  j['processors']=5345
  j['memory']=734

  try:
    alg = AlgorithmConfiguration.objects.get(id=ObjectId(projID)) # get returns a document only if there is only one
  except AlgorithmConfiguration.DoesNotExist :
    print ("ERROR: algorithm doesn't exist")
    return
  except AlgorithmConfiguration.MultipleObjectsReturned:
    print ("ERROR: there are multiple occurrences of the performance evaluation in the DB")
    return




  alg.performance    = j['performance']
  alg.energy         = j['energy']
  alg.processors     = j['processors']
  alg.memory         = j['memory']

  alg.save() # save in the DB a updated document containing the results of the evaluation


  return





#just for test purposes
def countsec (**kwargs):
  import time
  time.sleep(60)
  return







class EvalPerf(Resource):

    def get(self):
        print(g.args)


       # print (PowerPerformanceEval.objects.count())

        try:
          prjID  = ObjectId(g.args.get ("project_id"))
        except: # check the id, it must be a 24digit hex number
          return {"message": "the id format is wrong", "id": 1}, 400, {}

        try:
          onnxID = ObjectId(g.args.get ("algorithm_configuration_id"))
        except:
          return {"message": "the id format is wrong", "id": 3}, 400, {}


        print (repr(prjID))
        print (repr(onnxID))

        '''
        # retrive the perf_eval
        try:
          perf_eval   = PowerPerformanceEval.objects.get(Q(algorithm=str(onnxID)) & Q(project=str(prjID)))

          print (repr(perf_eval))
        except PowerPerformanceEval.DoesNotExist: # if the project doesn't exist return an error
          return {"message": "ERROR! The project doesn't exist", "id": 2}, 400, {}
        '''

        # retrive the algorithm
        try:
          alg   = AlgorithmConfiguration.objects.get(id=ObjectId(onnxID))

          print (repr(alg))
        except AlgorithmConfiguration.DoesNotExist: # if the project doesn't exist return an error
          return {"message": "ERROR! The algorithm doesn't exist", "id": 2}, 400, {}


        print (alg.to_json())

        return {'performance': alg.performance,'energy': alg.energy,'processors': alg.processors,'memory': alg.memory}, 200, None







    def post(self):


        print(g.args)
       # print (Project.objects.count())
        try:
          prjID  = ObjectId(g.args.get ("project_id"))
        except: # check the id, it must be a 24digit hex number
          return {"message": "the id format is wrong", "id": 1}, 400, {}

        try:
          onnxID = ObjectId(g.args.get ("algorithm_configuration_id"))
        except:
          return {"message": "the id format is wrong", "id": 3}, 400, {}

        try:
          archID  = ObjectId(g.args.get ("architecture_id"))
        except: # check the id, it must be a 24digit hex number
          return {"message": "the id format is wrong", "id": 1}, 400, {}


        callback_id= g.args.get("callback_id")

        print (repr(prjID))
        print (repr(onnxID))
        print (repr(archID))


        # retrive the project
        try:
          project   = Project.objects.get(id=prjID)

          print (repr(project))
        except Project.DoesNotExist: # if the project doesn't exist return an error
          return {"message": "ERROR! The project doesn't exist", "code": 2}, 400, {}

        # retrive the onnx model
        try:
          onnxModel = AlgorithmConfiguration.objects.get(id=onnxID)
          print (repr(onnxModel))
        except AlgorithmConfiguration.DoesNotExist:
          return {"message": "the onnx model doesn't exist", "id": 4}, 400, {}

        # retrive the architecture
        try:
          arch = Architecture.objects.get(id=archID)
          print (repr(arch))
        except Architecture.DoesNotExist:
          return {"message": "the architecture doesn't exist", "id": 4}, 400, {}

        with Connection(conn): # connection to REDIS DB
          q = Queue("power_perf",connection=conn)

          # enqueue a new job
          job = q.enqueue_call(func=execute_eval, result_ttl=600000, timeout=600000, kwargs={"archPath":arch.path, "onnxPath":onnxModel.path, "projectID":prjID, "project_id":prjID,  "algorithmID":onnxID, "callback_id":callback_id})



        elabID = job.id;


        return {"message": "Elaboration started", "job_id": elabID}, 200, None
