# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function

from flask import Flask, request, g

from . import Resource
from .. import schemas



# the following two imports are needed for create a new connection
from settings import settings
from db.mongo import MongoConnector



#the following line imports the mongoDBengine classes from the models.py file
#each class correspond to a collection in the DB (a collection is the analogue to a table.
#An entry in the collection is the analogue of a row in the table)
from models.models import *

from mongoengine.errors import *
from mongoengine.queryset.visitor import Q
from bson import ObjectId

from rq import Queue

import subprocess
import os
import logging

from worker import *


SHARED_DATA_FOLDER = "/opt/data/"

LOGGING_FOLDER = os.path.join(SHARED_DATA_FOLDER, "experiments/prj_%s/alg_%s/power_perf_logs/") # to be completed with the project_id

def execute_evaluation (**kwargs):
    from st_evaluation_lib import ST_perfeval

    project_id = kwargs.get("project_id", None)
    algorithm  = kwargs.get("algorithm")
    #print (os.getcwd())
    # set up logging
    log_folder = LOGGING_FOLDER%(project_id, str(algorithm.get_id()))
    if not os.path.exists(log_folder):
        os.makedirs(log_folder)
    log_name = 'log_POWER_PERF_EXEC_{date:%Y%m%d_%H%M%S}.txt'.format( date=datetime.datetime.now())
    logging.basicConfig(level=logging.INFO,
                format="%(asctime)s - %(levelname)s - %(message)s",
                datefmt="%Y-%m-%d %H:%M:%S",
                filename = log_folder + log_name,
                filemode='w')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s',
                                  datefmt='%Y-%m-%d %H:%M:%S')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)



    ONNXPath    = os.path.join(SHARED_DATA_FOLDER, algorithm.onnx)
    logging.info ("Algorithm id: %s" % str(algorithm.get_id()))
    logging.info ("Algorithm path: %s" % str(algorithm.onnx))
    
    

    #project_id  = kwargs.get("project_id", None)
    #algoID      = kwargs.get("algorithmID", None)

    callback_id = kwargs.get("callback_id", None)

    #import time
    # uncomment the following line to simulate a longer process
    #time.sleep(30)
    #print (os.listdir(SHARED_DATA_FOLDER))
    if not os.path.isfile(ONNXPath):
        logging.error ("ERROR: file %s doesn't exist" % ONNXPath)

    results = ST_perfeval.st_performance_evaluation(ONNXPath)

    # connect to the DB
    mongo = MongoConnector()


    import json


    try:
        
        algorithm.performance    = results['execution_time']
        algorithm.energy         = results['energy']
        algorithm.processors     = results['processors']
        algorithm.memory         = results['memory']

        algorithm.save() # save in the DB a updated document containing the results of the evaluation
        logging.info ("Results saved in the DB")     
       
        

    except Exception as e:
        logging.error ("ERROR: Evaluation failed!")
        logging.error (e)
      
      
    logging.info ("Evaluation results:")
    logging.info (str(results))
    


    # callback to the DSE Engine

    import requests
    url='http://%s/api/dse_engine/callback?callback_id=%s'%('dse_engine:5000',callback_id)
    data={"some_data": 0}

    r = requests.post(url,data=data)

    #print (r.status_code)
    #print(r.json())
    return



class PowerPerformanceEvaluations(Resource):

    def get(self):
        print(g.args)


        try:
          project_id  = ObjectId(g.args.get ("project_id"))
        except: # check the id, it must be a 24digit hex number
          return {"message": "the id format is wrong", "id": 1}, 400, {}

        try:
          algorithm_id = ObjectId(g.args.get ("algorithm_configuration_id"))
        except:
          return {"message": "the id format is wrong", "id": 3}, 400, {}


        print (repr(project_id))
        print (repr(algorithm_id))


        # retrive the algorithm
        try:
          alg   = AlgorithmConfiguration.objects.get(id=ObjectId(algorithm_id))

          print (repr(alg))
        except AlgorithmConfiguration.DoesNotExist:
          return {"message": "ERROR! The algorithm doesn't exist", "id": 2}, 400, {}


        print (alg.to_json())

        return {'performance': alg.performance,'energy': alg.energy,'processors': alg.processors,'memory': alg.memory}, 200, None







    def post(self):
        print(g.json)
        print(g.args)

        try:
            algorithm_id = ObjectId(g.json.get ("algorithm_id"))
        except:
            return {"message": "the id format is wrong", "id": 3}, 400, {}

        try:
            project_id  = ObjectId(g.json.get ("project_id"))
        except: # check the id, it must be a 24digit hex number
            return {"message": "the id format is wrong", "id": 1}, 400, {}

        try:
          archID  = ObjectId(g.json.get ("architecture_id"))
        except: # check the id, it must be a 24digit hex number
          return {"message": "the id format is wrong", "id": 1}, 400, {}


        callback_id = g.args.get("callback_id")

        # retrive the Algorithm
        try:
            algorithm = AlgorithmConfiguration.objects.get(id=algorithm_id)
            #print (algorithm)
        except AlgorithmConfiguration.DoesNotExist:
            print ("ERROR: algorithm doesn't exist")
            return {"message": "the onnx model doesn't exist", "id": 4}, 400, {}

        # retrive the architecture
        #arch = Architecture.objects.get(id=archID)
        try:
          arch = Architecture.objects.get(project=str(project_id)) #TODO: to be fixed with the query to project
          #print (arch)
        except Architecture.DoesNotExist:
          print ("ERROR: Architecture doesn't exist")
          return {"message": "the architecture doesn't exist", "id": 4}, 400, {}    



        with Connection(conn): # connection to REDIS DB
          q = Queue("power_perf",connection=conn)

          params = {"algorithm"     : algorithm,
                    "architecture"  : arch,
                    "project_id"    : project_id,
                    "callback_id"   : callback_id
                    }

          # enqueue a new job
          job = q.enqueue_call(func=execute_evaluation, result_ttl=600000, timeout=600000, kwargs=params)

        return {"message": "Elaboration started", "job_id": job.id}, 200, None
