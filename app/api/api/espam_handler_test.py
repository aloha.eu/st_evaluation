import subprocess
import os
from threading import Timer

def main():
    try:
        tstONNXPath = os.getcwd() + os.sep + "onnx/mnist/mnist.onnx"
        tstArchPath = os.getcwd() + os.sep + "architecture/fake_arch.json"
        cmd = ['java', '-jar', 'protobuf_interface.jar', '--evaluate', tstONNXPath]
        DEVNULL = os.open(os.devnull, os.O_WRONLY)
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True, stderr=DEVNULL)
        timer = Timer(36000, process.kill)
        try:
            timer.start()
            stdout, stderr = process.communicate()
        finally:
            timer.cancel()

        result = stdout
        print(result)
        return result


    except Exception:
        print("error")
        return "error"

if __name__ == "__main__":
    main()
