# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function

from flask import request, g

from . import Resource
from .. import schemas

from rq import Queue, Connection
from worker import *
from rq.job import Job

class EvalPerfStatus(Resource):

    def get(self):
        print(g.args)
        
        job_id=g.args.get("job_id")
        print (job_id)
        with Connection(conn):
          q = Queue(connection=conn)
          try:
            job = Job.fetch(job_id,connection=conn)
          except:
            return {'message': "No such job ID"}, 400, {}
          print (job)
          status= job.get_status()

        return {'message': status}, 200, None
