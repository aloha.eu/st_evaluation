from models.models import *
from flask import Flask
from db.mongo import MongoConnector # Required to access db in this way
import os


def save_test_proj_in_db():
    """
    1. CREATING FLASK APPLICATION + CONNECTING TO MongoDB
    """
    ## Creating Flask application to reproduce modules set up and environment.
    app = Flask(__name__)

    ###
    #  Configuring MongoDB access
    ###
    app.config['MONGODB_SETTINGS'] = {
        'db': 'aloha-test', #Name of the database
        'host': "localhost", # Host of the DB
        'port': 27017, # DB port
        #'username':'aloha',
        #'password':'pwd123'
    }

    ## Start a new connection with the database (Required to use MongoEngine).
    mongo = MongoConnector(app)
    db = mongo.db

    # get architecture description from the db
    archID = "5bc5cc5b4d8961248b37a429"
    arch = Architecture.objects.get(id=archID)
    #print (arch.path)

    """" 
        Save test data in db (if needed)
    """
    try:
        user = User.objects.get(email="svetlanaminakova42@gmail.com")
    except User.DoesNotExist:
        user = User()
        user.email = "svetlanaminakova42@gmail.com"
        user.save()
        user = User.objects.get(email="svetlanaminakova42@gmail.com")

    ##Save test ONNX example in DB, if not exist
    tstONNXPath = os.getcwd() + os.sep + "onnx/mnist/mnist.onnx"
    tstArchPath = os.getcwd() + os.sep + "architecture/fake_arch.json"

    # create new project
    proj = Project()
    proj.creator = user.get_id()
    proj.save()
    projID = proj.get_id()

    # create AlgorithmConfiguration if does not exist
    tstONNX = AlgorithmConfiguration.objects.filter(path=tstONNXPath)
    tstONNXID = None
    if tstONNX.count() > 0:
        tstONNXID = tstONNX.first().get_id()
    else:
        alg_file = AlgorithmConfiguration()
        alg_file.project = projID
        alg_file.path = tstONNXPath
        alg_file.save()
        tstONNXID = alg_file.get_id()

    # create Architecture if does not exist
    tstArch = Architecture.objects.filter(path=tstArchPath)
    tstArchID = None
    if tstArch.count() > 0:
        tstArchID = tstArch.first().get_id()
    else:
        tstArch = Architecture()
        tstArch.path = tstArchPath
        tstArch.project = projID
        tstArch.save()
        tstArchID = tstArch.get_id()

    "read test data from db and process it"
    #db access
    dnn_path = (AlgorithmConfiguration.objects.get(id=tstONNXID)).path
    print(dnn_path)
    arch_path = (Architecture.objects.get(id=tstArchID)).path
    print(arch_path)